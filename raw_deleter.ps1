﻿# RAW Deleter by Chris Meagher, 9 June 2019
#
# This script assumes it is run in a folder full of .JPG files and that
# another folder called RAW contains Canon RAW files (.CR2).  The script
# deletes any .CR2 files that do not have a corresponding .JPG.
#
# This is part of my photo editing workflow.  I capture both .JPG and .CR2
# and, since they load faster, manually review and cull the .JPG files.  This
# script then repeats the culling on the .CR2 RAW files, then I edit the 
# remaining "keeper" RAW files.
#
# If you don't want to run as powershell script but rather enter in the 
# individual commands, make a Powershell shortcut and change the "Start In"
# path to %CD% to make the shortcut start in the current directory.

$WorkingFolder = (Get-Item -Path ".\").FullName                                # This is the current directory with JPG files
$RAWFolder = $WorkingFolder + "\RAW"                                           # Assumes RAW files are in RAW subdirectory
$RAWFiles = Get-ChildItem $RAWFolder                                           # Get a list of the files in the RAW subdirectory

foreach ($RAWFile in $RAWFiles) {                                              # For each RAW file,
    $JPGFile = $WorkingFolder + "\"+ ($RAWFile -replace '\..*') + ".JPG";      # strip out the .CR2 in the file name and replace it with .JPG
    if (!(Test-Path $JPGFile -PathType Leaf)) {                                # and check whether the file exists.
        Remove-Item $RAWFile.FullName                                          # If not, delete the file.
    }; 
}
