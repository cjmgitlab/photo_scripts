# photo_scripts

## Description
These are scripts I have written over the years to help with automatically processing photos.

- The raw_deleter scripts (bash and PowerShell) assume they are run in a folder full of .JPG files and that a sub-folder called RAW contains Canon RAW files (.CR2).  The script deletes any .CR2 files that do not have a corresponding .JPG.  This is part of my photo editing workflow.  I capture both .JPG and .CR2 and, since they load faster, manually review and cull the .JPG files.  This script then repeats the culling on the .CR2 RAW files, then I edit the remaining "keeper" RAW files.

- The sort_landscape_portrait bash script takes a folder of pictures and puts them into sub-folders called "landscape" or "portrait" depending on whether their width is greater than their height or vice versa.  I use this to try to collect only landscape photos for a digital photo frame or to use the same orientation of photos for a collage.

## License
Apache License, Version 2.0

## Project status
The raw_deleter scripts are essentially done (I've been using them for a few years now).  The landscape/portrait sorting script still fails to detect the correct orientation sometimes so I expect to be revising that script.
