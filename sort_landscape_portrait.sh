#!/usr/bin/env bash

# Notes:
# - requires imagemagick
# - got from https://unix.stackexchange.com/questions/294341/shell-script-to-separate-and-move-landscape-and-portrait-images
# - renaming: https://unix.stackexchange.com/questions/19654/how-do-i-change-the-extension-of-multiple-files

# Make the portrait and landscape directories if they don't already exist
mkdir -p portrait
mkdir -p landscape

# Rename .JPG, .JPEG, and .jpeg to .jpg
for f in ./*.JPG; do
    mv -- "$f" "${f%.JPG}.jpg"
done

for f in ./*.JPEG; do
    mv -- "$f" "${f%.JPEG}.jpg"
done

for f in ./*.jpeg; do
    mv -- "$f" "${f%.jpeg}.jpg"
done

# Move the .jpg files to one of the two folders depending on the ratio of their height and width
for f in ./*.jpg
do
    r=$(identify -format '%[fx:(h>w)]' "$f")
    if [[ r -eq 1 ]] 
    then
        mv "$f" ./portrait/
    else
        mv "$f" ./landscape/
    fi
done

# Rename .PNG to .png
for f in ./*.PNG; do
    mv -- "$f" "${f%.PNG}.png"
done

# Move the .png files to one of the two folders depending on the ratio of their height and width
for f in ./*.png
do
    r=$(identify -format '%[fx:(h>w)]' "$f")
    if [[ r -eq 1 ]] 
    then
        mv "$f" ./portrait/
    else
        mv "$f" ./landscape/
    fi
done

# Rename .HEIC to .heic
for f in ./*.HEIC; do
    mv -- "$f" "${f%.HEIC}.heic"
done

# Move the .heic files to one of the two folders depending on the ratio of their height and width
for f in ./*.heic
do
    r=$(identify -format '%[fx:(h>w)]' "$f")
    if [[ r -eq 1 ]] 
    then
        mv "$f" ./portrait/
    else
        mv "$f" ./landscape/
    fi
done