#!/usr/bin/env bash
find ./RAW/* | while read f
do
	#echo $f
	#Need to get to .jpg file names.  First, cut out path
	jpg_filename=${f##*/}
	
	#Then strip out the .CR2 and add in .JPG
	jpg_filename=$(echo "$jpg_filename" | cut -f 1 -d '.')
	jpg_filename=$jpg_filename.JPG

	#echo $jpg_filename

	if [ ! -e $jpg_filename ]
	then
		rm $f
	fi
done
